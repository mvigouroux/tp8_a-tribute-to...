const text = document.querySelector('h1');
const audio = new Audio('./assets/audio/mysound.mp3');

function booh(){
    text.classList.add('booh');
}

text.addEventListener('click', booh);

window.onload = () => {

    if($('.booh') != null){

        const clickToAuthorize = document.querySelector('h1');
        const hoverToPlay = document.querySelector('.booh');
        
        clickToAuthorize.onclick = () => {
          hoverToPlay.style.display = 'block';
        }
      
        hoverToPlay.onmouseover = () => audio.play();
        hoverToPlay.onmouseout = () => audio.pause();
    }
}

function playSound(event) {

    audio.currentTime = 0;
    audio.volume = 0;
    setTimeout(function(){ audio.volume = 1; }, 500);
    audio.play();
}

function stopSound(event) {
    audio.pause();
    text.classList.remove('booh');
}

text.addEventListener('click', playSound);
text.addEventListener('mouseout', stopSound);


// text.addEventListener('mouseover', playSound(){
//     audio.play.bind(audio)
// })

// // get the hover element and instantiate sound.
// const hoverSoundElement = document.getElementsById('sound')
// , sound = new Audio("./")

// // play the sound when the mouse enters.
// hoverSoundElement.addEventListener("mouseover", sound.play.bind(sound))

// hoverSoundElement.addEventListener("mouseout", function() {

//   // stop the sound when the mouse leaves.
//   sound.pause()

//   // reset sound's position to the beginning.
//   sound.currentTime = 0
// })